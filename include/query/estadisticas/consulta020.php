<?php

/*****
 * @autor Jenny Gamez
 * Orfeo 6.0
 * Estadistica que muestra los radicados de PQRS que se encuentran en cada uno de los estados segun como corresponde y adicional indica
 * si el mismo se encuentra vencido porque no se haya mandado a archivar
 * ACTUALMENTE SOLO ESTA EN POSTGRES
 ****/

if($conrespuesta == 1){
    $sWconRespuesta = " and an.radi_nume_salida is not null and an.radi_nume_salida <> '0' ";
}
//Radicados sin respuesta
elseif($conrespuesta == 2){
    $sWconRespuesta = " and an.radi_nume_salida = '0' or an.radi_nume_salida is null";
}
else{
    $sWconRespuesta = " ";
}

// Toma como criterio de busqueda la dependencia
if ($dependencia_busq == '99999' && ($tipoDocumento == '9999' or $tipoDocumento == '9998' or $tipoDocumento == '9997') && $medioRecepcion == '9999') {
    $sWhere = "  ";
}
// Toma como criterio de busqueda de la dependencia 
elseif ($dependencia_busq != '99999' && ($tipoDocumento == '9999' or $tipoDocumento == '9998' or $tipoDocumento == '9997')  && $medioRecepcion == '9999') {
    $sWhere = " and ra.radi_depe_actu = '" . $dependencia_busq. "' ";
}
// Toma como criterio de busqueda del tipo documental
elseif ($dependencia_busq == '99999' && ($tipoDocumento != '9999' or $tipoDocumento != '9998' or $tipoDocumento != '9997')  && $medioRecepcion == '9999') {
    $sWhere = " and tp.sgd_tpr_codigo = $tipoDocumento";
}
// Toma como criterio de busqueda el medio de recepción
elseif ($dependencia_busq == '99999' && ($tipoDocumento == '9999' or $tipoDocumento == '9998' or $tipoDocumento == '9997')  && $medioRecepcion != '9999') {
    $sWhere = " and  ra.mrec_codi= $medioRecepcion";
}
// Toma como criterio de busqueda del tipo documental, y la dependencia
elseif ($dependencia_busq != '99999' && ($tipoDocumento != '9999' or $tipoDocumento != '9998' or $tipoDocumento != '9997')  && $medioRecepcion == '9999') {
    $sWhere = " and ra.radi_depe_actu = '" . $dependencia_busq. "' and tp.sgd_tpr_codigo = $tipoDocumento";
}
// Toma como criterio de busqueda la dependencia y el medio de recepción
elseif ($dependencia_busq != '99999' && ($tipoDocumento == '9999' or $tipoDocumento == '9998' or $tipoDocumento == '9997')  && $medioRecepcion != '9999') {
    $sWhere = " and  ra.mrec_codi= $medioRecepcion  and ra.radi_depe_actu = '" . $dependencia_busq. "'";
}
// Toma como criterio de busqueda el medio de recepción y el tipo documental
elseif ($dependencia_busq == '99999' && ($tipoDocumento != '9999' or $tipoDocumento != '9998' or $tipoDocumento != '9997')  && $medioRecepcion != '9999') {
    $sWhere = " and  ra.mrec_codi= $medioRecepcion and tp.sgd_tpr_codigo = " . $tipoDocumento;
}
// Toma como criterio de busqueda del tipo documental, la dependencia y medio de recepción
elseif ($dependencia_busq != '99999' && ($tipoDocumento != '9999' or $tipoDocumento != '9998' or $tipoDocumento != '9997')  && $medioRecepcion != '9999') {
    $sWhere = " and ra.radi_depe_actu = '" . $dependencia_busq. "' and tp.sgd_tpr_codigo = $tipoDocumento and ra.mrec_codi= $medioRecepcion";
}
else{
    $sWhere = " ";
}

if ($estadoRadicado != 'TODOS') {
    $sWhere .= " and es.esta_codi = $estadoRadicado";
} 

if (trim($tipoRadicado) != '') {
    $sWhere .= " and ra.radi_nume_radi like '%".$tipoRadicado."' and (anex_radicado=1 or an.anex_radi_nume is null or an.radi_nume_salida is null) ";
} else {
    $sWhere .= " and ( ra.radi_nume_radi like '%2' or ra.radi_nume_radi like '%4' ) and (anex_radicado=1 or an.anex_radi_nume is null or an.radi_nume_salida is null) "; // Entrada o PQRS
}

switch ($db->driver) {
    // ra.ra_asun, de.depe_nomb as dependencia_creadora, 
    case 'postgres': {
            $queryE = "select 
                        tp.sgd_tpr_descrip      as \"TIPO_DOCUMENTAL\",
                        ra.radi_nume_radi       as \"NUMERO_RADICADO\",
                        d.sgd_dir_nomremdes     as \"REMITENTE/DESTINATARIO\",
                        CASE 
                            WHEN an.radi_nume_salida is null 
                                THEN 'Sin respuesta' 
                                ELSE an.radi_nume_salida 
                        END                     as \"RESPUESTA\", 
                        deac.depe_nomb          as \"DEPENDENCIA_ACTUAL\", 
                        CASE 
                            WHEN deac.depe_codi = '0999' 
                                THEN deperes.depe_nomb 
                                ELSE deac.depe_nomb 
                        END                     as \"DEPENDENCIA_RESPONSABLE\",
                        mr.mrec_desc            as \"MEDIO_RECEPCION\",
                        ra.radi_fech_radi       as \"FECHA_RADICACION\",
                        ra.fech_vcmto           as \"FECHA_VENCIMIENTO\",
                        DATE_PART('day', an.anex_radi_fech::date) - DATE_PART('day', ra.radi_fech_radi::date) as tiempo_promedio, 
                        DATE_PART('day', ra.fech_vcmto::date) - DATE_PART('day', an.anex_radi_fech::date) as tiempo_vr_vencimiento, 
                        dt.dias_termino, 
                        CASE 
                            WHEN ra.radi_estado_pqrs is null
                                THEN 'No aplica'
                                ELSE es.esta_desc
                        END                     as \"ESTADO\",

                        ra.sgd_spub_codigo      as \"HID_SGD_SPUB_CODIGO\",
                        u.usua_login            as \"HID_USUA_LOGIN\",
                        u.usua_nivel_consulta   as \"HID_USUA_NIVEL_CONSULTA\"
                    from radicado ra 
                        inner join dependencia de on ra.radi_depe_radi=de.depe_codi 
                        inner join dependencia deac on ra.radi_depe_actu=deac.depe_codi 
                        inner join sgd_tpr_tpdcumento tp on ra.tdoc_codi =tp.sgd_tpr_codigo 
                        inner join medio_recepcion mr on ra.mrec_codi=mr.mrec_codi 
                        inner join usuario as u on ra.radi_usua_actu = u.usua_codi and ra.radi_depe_actu = u.depe_codi
                        left join estado es on es.esta_codi=ra.radi_estado_pqrs
                        left join sgd_dir_drecciones d on ra.radi_nume_radi=d.radi_nume_radi
                        left join anexos an on ra.radi_nume_radi=an.anex_radi_nume 
                        inner join sgd_dt_radicado dt on ra.radi_nume_radi=dt.radi_nume_radi 
                        left join dependencia deperes on deperes.depe_codi=ra.radi_depe_ante";

            $queryE .= " where " .$db->conn->SQLDate('Y/m/d', 'ra.radi_fech_radi') . " BETWEEN '$fecha_ini' AND '$fecha_fin' ". $sWhere;
            
        }break;
    case 'oci8': {
            $queryE = 'select '
                        . 'radi_nume_radi, '
                        . 'ra_asun, '
                        . 'radi_fech_radi, '
                        . 'de.depe_nomb, '
                        . 'us.usua_nomb, '
                        . 'tu.nombre_tipo_usuario, '
                        . 'tp.sgd_tpr_descrip '
                    . 'from radicado ra '
                    . 'inner join dependencia de on ra.radi_depe_radi=de.depe_codi '
                    . 'inner join usuario us '
                        . 'on ra.radi_usua_radi=us.usua_codi '
                        . 'and ra.radi_depe_radi=us.depe_codi '
                    . 'inner join tipo_usuario_grupo tu '
                        . 'on ra.tipo_usario_interes = tu.id_grupo_tipo_usuario '
                    . 'inner join sgd_tpr_tpdcumento tp '
                        . 'on ra.tdoc_codi =tp.sgd_tpr_codigo';
            $queryE .= " where " .$db->conn->SQLDate('Y/m/d', 'ra.radi_fech_radi') . " BETWEEN '$fecha_ini' AND '$fecha_fin' ". $sWhere;
        }break;
}

$titulos = array("#", "1#Tipo Documental", "2#Radicado", "3#Remitente/Destinatario", "4#Respuesta", "5#Dependencia Actual", "6#Dependencia Responsable", "7#Medio Recepción", "8#Fecha Radicación", "10#Fecha Vencimiento", "11#Días Promedio de Respuesta", "12#Días Antes Vencimiento", "13#Días de Respuesta Normativo", "14#Estado");

function pintarEstadistica($fila, $indice, $numColumna){
    global $ruta_raiz, $_POST, $_GET, $krd, $ambiente;

    $tipo_documental = $fila['TIPO_DOCUMENTAL'];
    $numero_radicado = $fila['NUMERO_RADICADO'];
    $respuesta = $fila['RESPUESTA'];
    $remi_des = $fila['REMITENTE/DESTINATARIO'];
    $dependencia_responsable = $fila['DEPENDENCIA_RESPONSABLE'];
    $dependencia_actual = $fila['DEPENDENCIA_ACTUAL'];
    $medio_recepcion = $fila['MEDIO_RECEPCION'];
    $fecha_radicacion =  date("Y-m-d", strtotime($fila['FECHA_RADICACION']));  
    $fecha_rec_remi = $fila['FECHA_REC_REMI'];
    $fech_vcmto =  date("Y-m-d", strtotime($fila['FECHA_VENCIMIENTO']));
    $tiempo_promedio = $fila['TIEMPO_PROMEDIO'];
    $tiempo_vr_vencimiento = $fila['TIEMPO_VR_VENCIMIENTO'];
    $dias_termino = $fila['DIAS_TERMINO'];
    $estado = $fila['ESTADO'];

    $fecha1 = new DateTime($fecha_radicacion);
    $fecha2 = new DateTime($fecha_rec_remi);

    if(!empty($fecha_rec_remi)){
        $promedio_respuesta = $fecha1->diff($fecha2);
        $tiempo_promedio = $promedio_respuesta->format('%R%a días');
    }else{
        $tiempo_promedio = "";
    }

    $verImg = ($fila['HID_SGD_SPUB_CODIGO'] == 1) ? ($fila['HID_USUA_LOGIN'] != $_SESSION['usua_nomb'] ? false : true) : ($fila['HID_USUA_NIVEL_CONSULTA'] > $_SESSION['nivelus'] ? false : true);


    $salida = "";
    switch ($numColumna) {
        case 0:
            $salida = $indice;
            break;
        case 1:
            $salida = $tipo_documental;
            break;
        case 2:
            $salida = $numero_radicado;
            break;
        case 3:
            $salida = $remi_des;
            break;
        case 4:
            $salida = $respuesta;
            break;
        case 5:
            $salida = $dependencia_actual;
            break;
        case 6:
            $salida = $dependencia_responsable;
            break;
        case 7:
            $salida = $medio_recepcion;
            break;
        case 8:
            if ($verImg) {
                $salida = "<a class=\"vinculos\" href=\"$url_raiz/$ambiente/verradicado.php?verrad=" . $numero_radicado . "&amp;" . session_name() . "=" . session_id() . "&amp;krd=" . $_GET['krd'] . "&amp;carpeta=8&amp;nomcarpeta=Busquedas&amp;tipo_carp=0 \" >" . $fecha_radicacion . "</a>";
            } else {
                $salida = $fecha_radicacion;
            }
            break;
        case 9:
            $salida = $fech_vcmto;
            break;
        case 10:
            if($tiempo_promedio != ''){
                $salida = $tiempo_promedio;
            }else{
                $salida = 'Sin respuesta';
            }
            break;
        case 11:
            if($tiempo_vr_vencimiento != ''){
                $salida = $tiempo_vr_vencimiento;
            }else{
                $salida = 'Sin respuesta';
            }
            break;
        case 12:
            $salida = $dias_termino;
            break;
        case 13:
            $salida = $estado;
            break;
        default:$salida = false;
    }
    return $salida;
}
?>
