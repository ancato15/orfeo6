<?php

session_start();
$dir_raiz = $_SESSION['dir_raiz'];
require_once("../../include/db/ConnectionHandler.php");
include "../../config.php";
$dbProcess = new ConnectionHandler("$dir_raiz");
$dbProcess->conn->SetFetchMode(ADODB_FETCH_ASSOC);
$dbProcess->conn->debug = false;

// include_once("../../include/PHPMailer/class.phpmailer.php");
include_once("../../include/PHPMailer5.2/PHPMailerAutoload.php");

//Instancia de mail para realizar las notificaciones a los usuarios
$mail = new PHPMailer();

//Incluido el archivo que tiene la clase historico
include("../../include/tx/Historico.php");
$historiRecord = new Historico($dbProcess);

//Lanzamiento auto
if (isset($_POST['type']) && $_POST['type'] == 100) {
    $selectDependencias = "";

    $queryDependencias = "SELECT DEPE_CODI, DEPE_NOMB, DEPE_CODI FROM dependencia";
    $rsDependencias = $dbProcess->conn->query($queryDependencias);

    $selectDependencias = "<select name='dependenciaReasignaMasiva' id ='dependenciaReasignaMasiva' class='select form-control'>";
    $selectDependencias .= "<option value='0'>-- Seleccione una dependencia --</option>";
    while (!$rsDependencias->EOF) {
        $selectDependencias .= "<option value='" . $rsDependencias->fields['DEPE_CODI'] . "'>" . $rsDependencias->fields['DEPE_CODI'] . " - " . $rsDependencias->fields['DEPE_NOMB'] . "</option>";

        $rsDependencias->MoveNext();
    }
    $selectDependencias .= "</select>";
    echo $selectDependencias;
}

//Type 1 = Consulta de los usuarios de acuerdo a la dependencia
if (isset($_POST['type']) && $_POST['type'] == 1) {
    $selectUsuarios = "";
    $count = 0;
    $queryUsuarios = "SELECT usua_login, usua_codi, depe_codi, usua_nomb, usua_doc FROM usuario WHERE depe_codi = '" . $_POST['dependencia'] . "'";
    $rsUsuarios = $dbProcess->conn->query($queryUsuarios);

    $selectUsuarios = "<select name='usuarios' id ='usuarios' class='select form-control'>";
    $selectUsuarios .= "<option value='0'>-- Seleccione un usuario --</option>";
    while (!$rsUsuarios->EOF) {
        $count++;
        $selectUsuarios .= "<option value='" . $rsUsuarios->fields['usua_login'] . "|" . $rsUsuarios->fields['usua_codi'] . "|" . $rsUsuarios->fields['usua_doc'] . "'>" . $rsUsuarios->fields['usua_nomb'] . "</option>";

        $rsUsuarios->MoveNext();
    }
    $selectUsuarios .= "</select>";

    if ($count > 0) {
        echo $selectUsuarios;
    } else {
        echo "No hay usuarios para la dependencia";
    }
}

//Type 2 = Consulta de los radicados de acuerdo a la dependencia
if (isset($_POST['type']) && $_POST['type'] == 2) {
    $tiposRadi = [];
    $tableRadicados = "";

    $queryTipoRadi = "SELECT sgd_trad_codigo, sgd_trad_descr FROM sgd_trad_tiporad";
    $rsTipoRadi = $dbProcess->conn->query($queryTipoRadi);
    while (!$rsTipoRadi->EOF) {
        $tiposRadi[$rsTipoRadi->fields['sgd_trad_codigo']] = $rsTipoRadi->fields['sgd_trad_descr'];

        $rsTipoRadi->MoveNext();
    }

    $queryUsuariosConsultarUsuaAnte = "SELECT usua_codi, usua_login, usua_nomb FROM usuario";
    $rsUsuariosConsultarUsuaAnte = $dbProcess->conn->query($queryUsuariosConsultarUsuaAnte);

    while (!$rsUsuariosConsultarUsuaAnte->EOF) {
        $usuaLogin[$rsUsuariosConsultarUsuaAnte->fields['usua_login']] = $rsUsuariosConsultarUsuaAnte->fields['usua_nomb'];

        $rsUsuariosConsultarUsuaAnte->MoveNext();
    }

    $usuarioPostExplode = explode('|', $_POST['usuario']);

    $queryRadicados = "SELECT radi_nume_radi, ra_asun, radi_depe_actu, radi_usua_actu, radi_usu_ante FROM radicado WHERE radi_depe_actu = '" . $_POST['dependencia'] . "' AND radi_usua_actu = '" . $usuarioPostExplode[1] . "' AND radi_nume_deri = '0'";
    $rsRadicados = $dbProcess->conn->query($queryRadicados);

    while (!$rsRadicados->EOF) {
        $tableRadicados .= "<tr>";
        $tableRadicados .= "<td>" . $rsRadicados->fields['radi_nume_radi'] . "</td>";
        $tableRadicados .= "<td>" . $rsRadicados->fields['ra_asun'] . "</td>";
        $tableRadicados .= "<td>" . $tiposRadi[substr($rsRadicados->fields['radi_nume_radi'], -1)] . "</td>";
        $tableRadicados .= "<td>" . $usuaLogin[$rsRadicados->fields['radi_usu_ante']] . "</td>";
        $tableRadicados .= "<td><input type='checkbox' name='" . $rsRadicados->fields['radi_nume_radi'] . "' id='" . $rsRadicados->fields['radi_nume_radi'] . "' class='inputCheckboxRadi' /></td>";
        $tableRadicados .= "</tr>";

        $rsRadicados->MoveNext();
    }
    echo $tableRadicados;
}

//Type 3 = Consulta de las dependencias para reasignar sin listar la dependencia consultada en el type 1
if (isset($_POST['type']) && $_POST['type'] == 3) {
    $selectDependenciasReasignar = "";

    //$queryDependenciasReasignar = "SELECT DEPE_NOMB, DEPE_CODI FROM dependencia WHERE DEPE_CODI != '". $_POST['dependencia'] ."'";
    $queryDependenciasReasignar = "SELECT DEPE_NOMB, DEPE_CODI FROM dependencia";
    $rsDependenciasReasignar = $dbProcess->conn->query($queryDependenciasReasignar);

    $selectDependenciasReasignar = "<select name='selectDependenciaReasignar' id ='selectDependenciaReasignar' class='select form-control'>";
    $selectDependenciasReasignar .= "<option value='0'>-- Seleccione una dependencia --</option>";
    while (!$rsDependenciasReasignar->EOF) {
        $selectDependenciasReasignar .= "<option value='" . $rsDependenciasReasignar->fields['DEPE_CODI'] . "'>" . $rsDependenciasReasignar->fields['DEPE_CODI'] . " - " . $rsDependenciasReasignar->fields['DEPE_NOMB'] . "</option>";

        $rsDependenciasReasignar->MoveNext();
    }
    $selectDependenciasReasignar .= "</select>";

    echo $selectDependenciasReasignar;
}

//Type 4 = Consulta de los usuarios de acuerdo a la dependencia a reasignar basado en la consulta del type 3
if (isset($_POST['type']) && $_POST['type'] == 4) {
    $countUsuariosReasignar = 0;
    $selectUsuariosReasignar = "";

    $usuarioPostExplode = explode('|', $_POST['usuario']);

    $queryUsuariosReasignar = "SELECT usua_codi, depe_codi, usua_nomb, usua_doc, usua_email FROM usuario WHERE usua_codi != '" . $usuarioPostExplode[1] . "' AND depe_codi = '" . $_POST['dependenciaReasignar'] . "'";
    $rsUsuariosReasignar = $dbProcess->conn->query($queryUsuariosReasignar);
    $selectUsuariosReasignar = "<select name='selectUsuariosReasignar' id ='selectUsuariosReasignar' class='select form-control'>";
    $selectUsuariosReasignar .= "<option value='0'>-- Seleccione un usuario --</option>";
    while (!$rsUsuariosReasignar->EOF) {
        $countUsuariosReasignar++;
        $selectUsuariosReasignar .= "<option value='" . $rsUsuariosReasignar->fields['usua_codi'] . "|" . $rsUsuariosReasignar->fields['usua_doc'] . "|" . $rsUsuariosReasignar->fields['usua_email'] . "'>" . $rsUsuariosReasignar->fields['usua_nomb'] . "</option>";

        $rsUsuariosReasignar->MoveNext();
    }

    $selectUsuariosReasignar .= "</select>";

    if ($countUsuariosReasignar > 0) {
        echo $selectUsuariosReasignar;
    } else {
        echo "No hay usuarios para la dependencia";
    }
}

//Type 5 = Update en la base de datos tabla radicado
if (isset($_POST['type']) && $_POST['type'] == 5) {
    $okTotalUpdate = true;
    $radicadosNoUpdate = "";
    $observacionHistorico = utf8_decode("Reasigación masiva del radicado");
    $radicadosReasignados = [];
    $sendMail = "";

    $usuarioOrigen = explode('|', $_POST['usuarioOrigen']);
    $usuarioReasignar = explode('|', $_POST['usuarioReasignar']);

    for ($i = 0; $i < count($_POST['radicadosAreasignar']); $i++) {
        $queryValidateHijos = "SELECT radi_nume_radi, radi_nume_deri FROM radicado WHERE radi_nume_deri = '" . $_POST['radicadosAreasignar'][$i] . "'";
        $rsValidateHijos = $dbProcess->conn->Execute($queryValidateHijos);
        $radicadosReasignados[] = $_POST['radicadosAreasignar'][$i];
        while (!$rsValidateHijos->EOF) {
            $radicadosReasignados[] = $rsValidateHijos->fields['radi_nume_radi'];

            $rsValidateHijos->MoveNext();
        }
    }

    for ($i = 0; $i < count($radicadosReasignados); $i++) {
        $queryUpdateRadicado = "UPDATE radicado SET radi_depe_actu = '" . $_POST['dependenciaReasignar'] . "', radi_usua_actu = '" . $usuarioReasignar[0] . "', radi_usu_ante = '" . $usuarioOrigen[0] . "', radi_fech_reasignado = '201-12-12', carp_codi = '14' WHERE radi_nume_radi = '" . $radicadosReasignados[$i] . "'";
        if ($dbProcess->conn->Execute($queryUpdateRadicado) == "") {
            $okTotalUpdate = false;
            $radicadosNoUpdate = $radicadosReasignados[$i] . ", ";
        } else {
            $radicadosReasignadosHis[] = $radicadosReasignados[$i];
        }
    }

    $historicoExec = $historiRecord->insertarHistorico($radicadosReasignadosHis, $_POST['dependenciaOrigen'], $usuarioOrigen[1], $_POST['dependenciaReasignar'], $usuarioReasignar[0], $observacionHistorico, 66);

    if ($okTotalUpdate) {
        foreach ($radicadosReasignadosHis as $noRadicado) {
            $radicadosMail .= $noRadicado . ", ";
        }

        $fecha = date("F j, Y H:i:s");
        $usMailSelect = $cuenta_mail;

        $textReasignaMasivaMayus = "REASIGNACIÓN MASVIA";
        $textReasignaMasivaMinus = "Reasigación masiva";
        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->SetFrom($usMailSelect, "SGD Orfeo 6");
        $mail->Host = $servidor_mail_smtp;
        $mail->Port = $puerto_mail_smtp;
        $mail->SMTPDebug = "0"; // 0 = off (for production use) // 1 = client messages // 2 = client and server messages
        $mail->SMTPAuth = "true";
        $mail->SMTPSecure = "";
        $mail->Username = $usMailSelect;   // SMTP account username
        $mail->Password = $contrasena_mail; // SMTP account password
        $mail->Subject = utf8_decode("Reasigación masiva de radicados");
        $mail->AltBody = "Para ver el mensaje, por favor use un visor de E-mail compatible!";
        $mail->AddAddress($usuarioReasignar[2]);

        $mensaje = "<html>
                <head>
                <title>" . $textReasignaMasivaMayus . "</title>
                </head>
                <body><p>
                " . $entidad . " , " . $fecha . " <br>
                <br></br>
                Se le informa que se le han reasignado radicados de manera masiva </b> en el Sistema de Gesti&oacute;n Documental Orfeo. Ingrese ";
        $mensaje .= 'radicados reasignados ' . $radicadosMail;

        $mensaje .= "<br></br>
                <br>Asunto: " . $textReasignaMasivaMinus . "</br>
                <br>
                <br>Cordialmente, </br>
                <br>Sistema de Gestion Documental Orfeo
                </p>
                </body>
                </html>
                ";
        $mail->MsgHTML(utf8_decode($mensaje));
        $exito = $mail->Send();

        if (!$exito) {
            $sendMail = utf8_decode('No se pudo enviar notificación de la reasignación masiva.');
        } else {
            $sendMail = utf8_decode('Se notificó la reasignación masiva');
        }
    }

    if ($okTotalUpdate) {
        echo "Radicados reasignados correctamente+++, Los radicados" . $radicadosNoUpdate . $sendMail;
    } else {
        echo "Radicados reasignados correctamente.\nLos radicados " . $radicadosNoUpdate . " no fueron procesados, " . $sendMail;
    }
}
?>
